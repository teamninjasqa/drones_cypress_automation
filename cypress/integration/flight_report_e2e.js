var createReportData = '../fixtures/create_report.json';
import manageAircraftCollection from '../plugins/common/function_collectors/collection_report_flight';
import createAircraft from "../support/objectMapper/create_report_map";
import { findstatus } from '../fixtures/export'
describe('Create and UpdateAircraft', () => {
    let navigateData = new createAircraft({});
    let SearchData = new createAircraft({});
    let otherData = new createAircraft({});
    let reportCollection = new manageAircraftCollection();

    before(() => {
        cy.fixture(createReportData).then((fetchReportData) => {
            navigateData = new createAircraft(fetchReportData[0].navigate)
            SearchData = new createAircraft(fetchReportData[0].report_aircraft_details)
            otherData = new createAircraft(fetchReportData[0].other_details)
        })
        const todaysDate = Cypress.moment().format('MM/DD/YYYY')
        findstatus.today_date = todaysDate;
        cy.log(todaysDate)

        secure();

    })

    it('Create and Manage Aircraft', () => {
        secure();
        reportCollection.navigateManageAircraft(navigateData.cr_manage_report_url);
        reportCollection.searchManageAircraft(SearchData.cr_search_name);
        cy.log(findstatus.today_date)
        reportCollection.createAircraftReport(SearchData.cr_flight_count, SearchData.cr_Duration_per_flight, SearchData.cr_risk_assessment, SearchData.cr_observer, SearchData.cr_takeoff_damages);

        //report checkbox
        reportCollection.userEquipmentMalfunctionsReport(otherData.malfunction_details);
        reportCollection.userLostLinkEvents(otherData.lostlink_details);
        reportCollection.userAccedentMishapReport(otherData.other_accident);

    });

});


function secure () {
    cy.setCookie('authtoken', 'eyJrZXlpZCI6Imh3cHJvcHUiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJVQ1JTUyIsImxhc3ROYW1lIjoiUGFsYW5pY2hhbXkiLCJmaXJzdE5hbWUiOiJBcmF2aW50aCIsInVzYWdlIjoiVVNFUiIsImlzcyI6Imh0dHA6Ly9kZXYucmlza2FuZHNhZmV0eS5jb20vZXJtc3AiLCJpYXQiOjE1NjMxNzQyNjksInRlbmFudCI6IlVDIiwiZW50aXR5IjoiMDMiLCJlbWFpbCI6ImFwYWxhbmljaGFteUB1Y2RhdmlzLmVkdSIsImNpZCI6InBhcnZpbmRAdWNkYXZpcy5lZHUiLCJqdGkiOiIyYzk2OTQ0Mi04MTZiLTQ4MzctODA2NS0zODdiYWM4ZmE1NTYiLCJzdWIiOiJWVU44TUROOGNHRnlkbWx1WkVCMVkyUmhkbWx6TG1Wa2RRIiwiZXhwIjoxNTYzMjYwNjY5fQ.Td0SquFzSmBbDLwS-vDsJkEgA8OCJUNSSv6kYwKIqiRUrCiwAv1Z11Fa8Cprkm-qs-VWBAGU6h1FD7f4rirWNkPNdRmUGvGcaq1qJCjbvQVSkVoIy6NzRnajLIVDe8UQsMpJD8L1UncTZSwS_7yiNgWOGCRYj6kx5tFsskbgGgjZzyxIth8QwxJ56CZwGJYvuUDSYWRphvmRAYzVH6C61aMcujlFEQFfycOXc3pbAhzjyDtO3v-s8BFDRG33KCtjPnvThE2arjQczL2__wb9d9cw9-qhbZMAIvnYI_jflBlsLyrnbwrxMLE_pVxeL3kwyeaByrg9_MdjTM7iP7zwdQ');
}
/* function randommath () {
    var textArray = ["[data-value='Non-Conforming']", "[data-value='Partially Conforming']", "[data-value='Substantially Conforming']", "[data-value='Conforming']", "[data-value='NA']"];
    returntextArray[Math.floor(Math.random() * textArray.length)];
} */
