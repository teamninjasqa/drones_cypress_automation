import createAircraft from "../support/objectMapper/create_aircraft_map";
var createData = '../fixtures/create_aircraft.json';
var updateData = '../fixtures/update_aircraft.json';
import manageAircraftCollection from '../plugins/common/function_collectors.js/collection_manage_aircraft';
import updateAircraft from "../support/objectMapper/update_aircraft_map";

describe('Create and UpdateAircraft', () => {
    let navigateData = new createAircraft({});
    let createAircraftData = new createAircraft({});
    let updateAircraftData = new updateAircraft({});
    let aircraftCollection = new manageAircraftCollection();

    before(() => {
        cy.fixture(createData).then((fetchCreateData) => {
            navigateData = new createAircraft(fetchCreateData[0].navigate)
            createAircraftData = new createAircraft(fetchCreateData[0].aircraft_details)
        })

        cy.fixture(updateData).then((fetchUpdateData) => {
            updateAircraftData = new updateAircraft(fetchUpdateData[0].update_details)
        })
    })

    it('Create and Manage Aircraft', () => {
        aircraftCollection.navigateManageAircraft(navigateData.cr_manage_flight_url);
        aircraftCollection.addManageAircraft(createAircraftData.cr_manufacturer_name, createAircraftData.cr_responsible_person, createAircraftData.cr_model_name, createAircraftData.cr_faa_registration, createAircraftData.cr_storage_location, createAircraftData.cr_campus_index, createAircraftData.cr_nickname)

        aircraftCollection.submitAircraft();
    });

    it('Update Aircraft', () => {
        aircraftCollection.navigateManageAircraft(navigateData.cr_manage_flight_url);
        aircraftCollection.updateManageAircraft(createAircraftData.cr_nickname, updateAircraftData.up_manufacturer_name, updateAircraftData.up_responsible_person, updateAircraftData.up_model_name, updateAircraftData.up_faa_registration, updateAircraftData.up_storage_location, updateAircraftData.up_nickname)

        aircraftCollection.submitAircraft();
    });

});