var ProjectData = '../fixtures/project_data.json';
var EditData = '../fixtures/edit_project_data.json'
import * as config from '../support/config'
import PageActions from '../plugins/common/page_actions';
import createProject from '../support/objectMapper/create_project_map'
import editProjectRequest from '../support/objectMapper/edit_project_map';
import { findstatus } from '../fixtures/export.js';
import d_project_create from '../support/page_objects/d_project_create'



describe('Login to Drones Dev', () => {
    let pageActions = new PageActions();
    let projectInfo = new createProject({})
    let editProject = new editProjectRequest({})
    let todayDate = Cypress.moment().format('MM-DD-YYYY');
    let modifiedDate = Cypress.moment().format('MM-DD-YYYY');

    before(() => {

        cy.fixture(ProjectData).then((projectCreateData) => {
            projectInfo = new createProject(projectCreateData[0])
        })

        cy.fixture(EditData).then((editProjectData) => {
            editProject = new editProjectRequest(editProjectData[0])
        })

        cy.fixture('../fixtures/find_status.json').as('finds')

        pageActions.resetApp().then((win) => {
            setToken();
            pageActions.doVisit(config.DevURL.devurl);
            findstatus.login_success = true;
        })
    })

    it('Draft create project', () => {
            setToken();
            projectRequestForm(pageActions, projectInfo, todayDate);

    });

    if (findstatus.draft_drones_project != null && findstatus.draft_drones_project != false) {
        it('Edit Project Request', () => {
            setToken();
            pageActions.containsClick(d_project_create.Project_request.edit_project)
            editRequestFrom(pageActions, editProject, modifiedDate)

        });
    }

    it('Submit Project Request', () => {
        setToken();
        cy.wait(5000)
        pageActions.containsClick(d_project_create.Project_request.submit_request);
        cy.wait(5000)
        pageActions.getElement(d_project_create.inreview.search_project);

    });

    it('Review Project Request', () => {
        setToken();
        // cy.visit('https://dev.riskandsafety.com/drones/project');
        /* (cy.contains('Test Automation 1 Edit').contains('In Review')).then(() => {
            cy.log("tfessssfsdfsd")

            cy.contains('Test Automation 1 Edit').contains('In Review').click()
        })
        
         */
        /*

        if (cy.contains(editProject.edit_project_name) && cy.contains('In Review')) {
            cy.log("tfessssfsdfsd")

        } */

        projectReviewer(pageActions, editProject);

    });

    it('Approve Project Request', () => {
        setToken();
        projectApprover(pageActions, editProject);

    });

});


function projectApprover (pageActions, editProject) {
    pageActions.getElement(d_project_create.Create.create_prj_btn);
    pageActions.EnterTextClear(d_project_create.inreview.search_project, editProject.edit_project_name);
    pageActions.listSelect(d_project_create.inreview.project_status, editProject.approve_project_status);
    pageActions.getElement(d_project_create.inreview.list_wait);
    pageActions.containsClick(editProject.edit_project_name);
    pageActions.containsClick(d_project_create.Project_request.risk_score);
    pageActions.containsClick(d_project_create.Project_request.approve_risk);
}

function projectReviewer (pageActions, editProject) {
    pageActions.EnterTextClear(d_project_create.inreview.search_project, editProject.edit_project_name);
    pageActions.listSelect(d_project_create.inreview.project_status, editProject.edit_project_status);
    pageActions.getElement(d_project_create.inreview.list_wait);
    pageActions.containsClick(editProject.edit_project_name);
    pageActions.enterText(d_project_create.Project_request.submit_comments, editProject.edit_reviewer_comments);
    pageActions.clickButton(d_project_create.Project_request.submit_project);
}

function setToken () {
    cy.setCookie('authtoken', 'eyJrZXlpZCI6Imh3cHJvcHUiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJVQ1JTUyIsImxhc3ROYW1lIjoiUGFsYW5pY2hhbXkiLCJmaXJzdE5hbWUiOiJBcmF2aW50aCIsInVzYWdlIjoiVVNFUiIsImlzcyI6Imh0dHA6Ly9kZXYucmlza2FuZHNhZmV0eS5jb20vZXJtc3AiLCJpYXQiOjE1NjM4NjI2NjEsInRlbmFudCI6IlVDIiwiZW50aXR5IjoiMDMiLCJlbWFpbCI6ImFwYWxhbmljaGFteUB1Y2RhdmlzLmVkdSIsImNpZCI6InBhcnZpbmRAdWNkYXZpcy5lZHUiLCJqdGkiOiI4YjNjYmE5Yy0yYTViLTQ2MjItOTg2Yi0zMTM5YTkzNzgzYjUiLCJzdWIiOiJWVU44TUROOGNHRnlkbWx1WkVCMVkyUmhkbWx6TG1Wa2RRIiwiZXhwIjoxNTYzOTQ5MDYxfQ.q7x2c_clIkpGxUtSbwZFCKApo0UA-1deC14_cYsoEl9ThoKb58N3iAEMrRZrpaIeK5SoYx40aCONSYWw98RKJVJutVGUw8wvUgvPj3lBTVmtJhGKsG-yWi_RHfL7etl4cEUKvsrh1HwXIOz3yvtlJHWWwyB_xzKU2PrJ6BXVg5-xJyB3sWr3N1Xpt7huOST6NB801w-9qm9FTigCQbCiJVlqbJdPo0MdzDDYlIK5nvE7eOJwagqLBC_780vElI-6GaOFC2dELUpiabPcr0jZV-a0gGpLj09TQeYxSo1a_aYnPSCjL6cfgHzeiZ_u7At3XVWOhSvP2-yP0pPRm0HWLA');
}

function projectRequestForm (pageActions, projectInfo, todayDate) {
    pageActions.doVisit(projectInfo.dev_project_create)
    pageActions.getElement(d_project_create.Create.verify_page)
    pageActions.doVisit(projectInfo.dev_project_create)
    pageActions.clickButton(d_project_create.Create.create_prj_btn);
    pageActions.enterText(d_project_create.Create.enter_aircraft_name, projectInfo.aircraft_name);
    pageActions.clickButton(d_project_create.Create.aircraft_autopop_sel);
    pageActions.enterText(d_project_create.Create.pilot_search_name, projectInfo.pilot_name);
    pageActions.clickButton(d_project_create.Create.aircraft_autopop_sel);
    pageActions.enterText(d_project_create.Project_request.project_name, projectInfo.d_project_name);
    pageActions.listSelect(d_project_create.Project_request.project_purpose, projectInfo.d_project_purpose);
    pageActions.listSelect(d_project_create.Project_request.uas_regulation, projectInfo.d_uas_regulation);
    pageActions.EnterTextClear(d_project_create.Project_request.project_start_date, todayDate)
    pageActions.EnterTextClear(d_project_create.Project_request.project_end_date, projectInfo.d_project_end_date);
    pageActions.EnterTextClear(d_project_create.Project_request.field_time, projectInfo.d_field_time);
    pageActions.listSelect(d_project_create.Project_request.frequency, projectInfo.d_frequency);
    pageActions.listSelect(d_project_create.Project_request.expected_range, projectInfo.d_expected_range);
    pageActions.clickButton(d_project_create.Project_request.latitude);
    pageActions.EnterTextClear(d_project_create.Project_request.latitude, projectInfo.d_latitude);
    pageActions.clickButton(d_project_create.Project_request.longitude);
    pageActions.EnterTextClear(d_project_create.Project_request.longitude, projectInfo.d_longitude);
    pageActions.EnterTextClear(d_project_create.Project_request.distance_cover, projectInfo.d_max_distance);
    pageActions.EnterTextClear(d_project_create.Project_request.project_altitude, projectInfo.d_flight_altitude);
    pageActions.EnterTextClear(d_project_create.Project_request.flight_procedure, projectInfo.d_flightProcedure)
    pageActions.radioButton(d_project_create.Project_request.flying_over, projectInfo.d_condition_index);
    pageActions.radioButton(d_project_create.Project_request.near_building, projectInfo.d_condition_index);
    pageActions.radioButton(d_project_create.Project_request.flying_indoor, projectInfo.d_condition_index);
    pageActions.containsClick(projectInfo.d_op_restriction_rain);
    pageActions.containsClick(projectInfo.d_op_restriction_trees);
    pageActions.containsClick(projectInfo.d_op_restriction_wind);
    pageActions.containsClick(projectInfo.d_op_TRFs);
    pageActions.containsClick(projectInfo.d_op_NOTAMS);
    pageActions.containsClick(projectInfo.d_op_Conflicting);
    pageActions.enterText(d_project_create.Project_request.observers_details, projectInfo.d_observer);
    pageActions.enterText(d_project_create.Project_request.risk_assessment, projectInfo.d_risk_assessment);
    pageActions.enterText(d_project_create.Project_request.user_comments, projectInfo.d_user_comments);
    pageActions.clickButton(d_project_create.Project_request.submit_project);
}

function editRequestFrom (pageActions, editProject, modifiedDate) {
    pageActions.enterText(d_project_create.Create.pilot_search_name, editProject.edit_pilot_name);
    pageActions.clickButton(d_project_create.Create.aircraft_autopop_sel);
    pageActions.EnterTextClear(d_project_create.Project_request.project_name, editProject.edit_project_name);
    pageActions.listSelect(d_project_create.Project_request.project_purpose, editProject.edit_project_purpose);
    pageActions.listSelect(d_project_create.Project_request.uas_regulation, editProject.edit_uas_regulation);
    pageActions.EnterTextClear(d_project_create.Project_request.project_start_date, modifiedDate);
    pageActions.EnterTextClear(d_project_create.Project_request.project_end_date, editProject.edit_project_end_date);
    pageActions.EnterTextClear(d_project_create.Project_request.field_time, editProject.edit_field_time);
    pageActions.listSelect(d_project_create.Project_request.frequency, editProject.edit_frequency);
    pageActions.listSelect(d_project_create.Project_request.expected_range, editProject.edit_expected_range);
    pageActions.clickButton(d_project_create.Project_request.latitude);
    pageActions.EnterTextClear(d_project_create.Project_request.latitude, editProject.edit_latitude);
    pageActions.clickButton(d_project_create.Project_request.longitude);
    pageActions.EnterTextClear(d_project_create.Project_request.longitude, editProject.edit_longitude);
    pageActions.EnterTextClear(d_project_create.Project_request.distance_cover, editProject.edit_max_distance);
    pageActions.EnterTextClear(d_project_create.Project_request.project_altitude, editProject.edit_flight_altitude);
    pageActions.EnterTextClear(d_project_create.Project_request.flight_procedure, editProject.edit_flightProcedure)
    pageActions.radioButton(d_project_create.Project_request.flying_over, editProject.edit_condition_index);
    pageActions.radioButton(d_project_create.Project_request.near_building, editProject.edit_condition_index);
    pageActions.radioButton(d_project_create.Project_request.flying_indoor, editProject.edit_condition_index);
    pageActions.containsClick(editProject.edit_op_restriction_rain);
    pageActions.containsClick(editProject.edit_op_restriction_trees);
    pageActions.EnterTextClear(d_project_create.Project_request.observers_details, editProject.edit_observer);
    pageActions.EnterTextClear(d_project_create.Project_request.risk_assessment, editProject.edit_risk_assessment);
    pageActions.EnterTextClear(d_project_create.Project_request.user_comments, editProject.edit_user_comments);
    pageActions.clickButton(d_project_create.Project_request.submit_project);
}