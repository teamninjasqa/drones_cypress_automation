var PilotData = '../fixtures/create_pilot_data.json';
var EditPilotData = '../fixtures/edit_pilot_data.json'
import * as config from '../support/config';
import PageActions from '../plugins/common/page_actions';
import createPilot from '../support/objectMapper/create_pilot_map';
import editPilot from '../support/objectMapper/edit_pilot_map';
import { findstatus } from '../fixtures/export.js';
import d_create_pilot from '../support/page_objects/po_pilot_create';


describe('Login to Radiation Dev', () => {
    let pageActions = new PageActions();
    let pilotInfo = new createPilot({})
    let pilotCreateInfo = new createPilot({})
    let editpilotInfo = new editPilot({})
    let editCertInfo = new editPilot({})

    let userId = "VUN8MDN8a2NoaW5lbkB1Y2RhdmlzLmVkdQ";
    const authpath = {
        "Authorization": "Bearer eyJrZXlpZCI6Imh3cHJvcHUiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkFyYXZpbnRoIiwic3ViIjoiVlVOOE1ETjhZWEJoYkdGdWFXTm9ZVzE1UUhWalpHRjJhWE11WldSMSIsInVzYWdlIjoiVVNFUiIsImlzcyI6Imh0dHA6Ly9kZXYucmlza2FuZHNhZmV0eS5jb20vZXJtc3AvIiwiYXVkIjoiVUNSU1MiLCJmaXJzdE5hbWUiOiJQYWxhbmljaGFteSIsImV4cCI6MTU2MDI5NzYwMCwiaWF0IjoxNTYwMjU2ODY4LCJ0ZW5hbnQiOiJVQyIsImVudGl0eSI6IjAzIiwiZW1haWwiOiJhcGFsYW5pY2hhbXlAdWNkYXZpcy5lZHUiLCJqdGkiOiIzMzViNGI5ZS04OGE2LTQ1MDUtYjJmMC1mN2ExNDBiOTFmNzEiLCJjaWQiOiJhcGFsYW5pY2hhbXlAdWNkYXZpcy5lZHUifQ.fvlOTi30aVIuY7wkLb6s1suQPIJA3GuYY3cvoc-nNl0KIDi3ORE8qhRgDFWDprWDsrDlj7Q2vo-64ezWPF-TSA"
    };

    before(() => {

        cy.fixture(PilotData).then((pilotDataFetch) => {
            pilotInfo = new createPilot(pilotDataFetch[0])
            pilotCreateInfo = new createPilot(pilotDataFetch[1])
        })

        cy.fixture(EditPilotData).then((editDataFetch) => {
            editpilotInfo = new editPilot(editDataFetch[0])
            editCertInfo = new editPilot(editDataFetch[1])
        })

        
        cy.fixture('../fixtures/find_status.json').as('finds')

        pageActions.resetApp().then((win) => {
            // win.sessionStorage.clear()
            pageActions.doVisit(config.DevURL.devurl);

            findstatus.login_success = true;
        })
    })

    it('Navigate to create pilot', () => {

        pageActions.getElement(d_create_pilot.pilot_creation.validate_homepage);
        pageActions.doVisit(d_create_pilot.pilot_creation.create_pilot_url);

    });


    it('Search for user and create pilot request', () => {

        createPilotDetails(pageActions, pilotInfo, pilotCreateInfo);
       
        searchPilotDetails(pageActions, editpilotInfo);

        changePilotDetails(pageActions, editCertInfo);
    });

    it('update pilot details', () => {

    });

   /*  it('Edit Pilot Details', () => {
        pageActions.listSelect(d_create_pilot.pilot_creation.select_campus, editpilotInfo.d_campus_selection);
        pageActions.enterText(d_create_pilot.pilot_creation.serach_pilots, editpilotInfo.d_pilot_name);
        pageActions.getElement(d_create_pilot.search_pilot.pilot_name_autopop)

    }); */
    /*  
     if (findstatus.draft_drones_project != null && findstatus.draft_drones_project != false) {
         it('Edit Project Request', () => {
             pageActions.containsClick(d_project_create.Project_request.edit_project)
             editRequestFrom(pageActions, editProject)
 
         });
     }
  */
});

function createPilotDetails (pageActions, pilotInfo, pilotCreateInfo) {
    pageActions.clickButton(d_create_pilot.pilot_creation.create_plus_button);
    pageActions.listSelect(d_create_pilot.pilot_creation.select_campus, pilotInfo.d_campus_selection);
    pageActions.enterText(d_create_pilot.pilot_creation.enter_pilot_tocreate, pilotInfo.d_pilot_to_create);
    pageActions.containsClick(d_create_pilot.pilot_creation.select_pilot_autopop);
    pageActions.listSelect(d_create_pilot.pilot_details.certification_type, pilotCreateInfo.d_cert_type);
    pageActions.EnterTextClear(d_create_pilot.pilot_details.certificate_name, pilotCreateInfo.d_cert_no);
    pageActions.EnterTextClear(d_create_pilot.pilot_details.pilot_phone, pilotCreateInfo.d_pilot_phone);
    pageActions.containsClick(d_create_pilot.pilot_details.Qualify_GS);
    pageActions.containsClick(d_create_pilot.pilot_details.Qualify_pilot);
    pageActions.clickButton(d_create_pilot.pilot_details.submit_pilot);
    pageActions.getElement(d_create_pilot.pilot_creation.create_plus_button);
}

function searchPilotDetails (pageActions, editpilotInfo) {
    pageActions.listSelect(d_create_pilot.pilot_creation.select_campus, editpilotInfo.d_campus_selection);
    pageActions.enterText(d_create_pilot.pilot_creation.serach_pilots, editpilotInfo.d_pilot_name);
    pageActions.getElement(d_create_pilot.search_pilot.pilot_name_autopop);
    pageActions.containsClick(editpilotInfo.d_inline_select);
    pageActions.getElement(d_create_pilot.pilot_details.submit_pilot);
}

function changePilotDetails (pageActions, editCertInfo) {
    pageActions.clickButton(d_create_pilot.pilot_details.certificate_name);
    pageActions.listSelect(d_create_pilot.pilot_details.certification_type, editCertInfo.d_cert_type);
    pageActions.EnterTextClear(d_create_pilot.pilot_details.certificate_name, editCertInfo.d_cert_no);
    pageActions.EnterTextClear(d_create_pilot.pilot_details.pilot_phone, editCertInfo.d_pilot_phone);
    pageActions.containsClick(d_create_pilot.edit_pilot_QF.Qualify_instructor);
    pageActions.clickButton(d_create_pilot.pilot_details.submit_pilot);
}
