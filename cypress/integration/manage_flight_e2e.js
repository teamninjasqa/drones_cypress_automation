
var manageFlightData = "../fixtures/manage_flight.json";
var updateFlightData = "../fixtures/update_manage_flight.json";
import manageFlight from '../support/objectMapper/manage_flight_map'
import manageFlightCollection from '../plugins/common/function_collectors.js/collection_manage_flight';
import updateFlight from '../support/objectMapper/update_flight_map';



describe('Manage Flight Save and Update scenarios', () => {

    let flightCollection = new manageFlightCollection();
    let navigateUrl = new manageFlight({});
    let addFlightData = new manageFlight({});
    let updateManageFlight = new updateFlight({});


    before(() => {
        cy.fixture(manageFlightData).then((fetchManageFlight) => {
            navigateUrl = new manageFlight(fetchManageFlight[0].navigate);
            addFlightData = new manageFlight(fetchManageFlight[0].add_manage_flight);
        })

        cy.fixture(updateFlightData).then((fetchUpdateData) => {
            updateManageFlight = new updateFlight(fetchUpdateData[0].update_manage_flight)
        })
    })

    /* it('Naviage to manage flight', () => {
        flightCollection.navigateFlight(navigateUrl.manage_flight_url);
    });
 */
    it('Create flights', () => {
        flightCollection.addManageFlight(navigateUrl.manage_flight_url, addFlightData.mfd_aircraft_name, addFlightData.mfd_pilot_name, addFlightData.mfd_nickname, addFlightData.mfd_flight_purpose, addFlightData.mfd_uas_regulation, addFlightData.mfd_flight_date, addFlightData.mfd_flight_time, addFlightData.mfd_field_time, addFlightData.mfd_no_of_flights, addFlightData.mfd_latitude, addFlightData.mfd_longitude, addFlightData.mfd_max_distance, addFlightData.mfd_flight_altitude, addFlightData.mfd_condition_index, addFlightData.mfd_flightProcedure, addFlightData.mfd_op_restriction_wind, addFlightData.mfd_op_restriction_trees, addFlightData.mfd_op_restriction_rain, addFlightData.mfd_risk_assessment, addFlightData.mfd_observer, addFlightData.mfd_user_comments);
    });

    it('update flight reqeust', () => {
        flightCollection.updateManageFlight(updateManageFlight.ed_mfd_pilot_name, updateManageFlight.ed_mfd_nickname, updateManageFlight.ed_mfd_flight_purpose, updateManageFlight.ed_mfd_uas_regulation, updateManageFlight.ed_mfd_flight_date, updateManageFlight.ed_mfd_flight_time, updateManageFlight.ed_mfd_field_time, updateManageFlight.ed_mfd_no_of_flights, updateManageFlight.ed_mfd_latitude, updateManageFlight.ed_mfd_longitude, updateManageFlight.ed_mfd_max_distance, updateManageFlight.ed_mfd_flight_altitude, updateManageFlight.ed_mfd_condition_index, updateManageFlight.ed_mfd_flightProcedure, updateManageFlight.ed_mfd_op_Conflicting, updateManageFlight.ed_mfd_op_TRFs, updateManageFlight.ed_mfd_op_NOTAMS, updateManageFlight.ed_mfd_observer, updateManageFlight.ed_mfd_risk_assessment, updateManageFlight.ed_mfd_user_comments)
        flightCollection.submitFlightReqest()
    });
});