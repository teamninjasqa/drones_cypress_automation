module.exports = {
    ConfigTimeout: [
        {
            "defaultCommandTimeout": 1000000,
            "pageLoad": 200000
        }

    ],

    DevURL:
    {
        "devurl": "https://dev.riskandsafety.com/drones/",
        "qaurl": "https://qa.riskandsafety.com/drones/",
        //   "devurl": "https://internal.itsvc.ucdavis.edu/radiation-dev",
        "login": 'null'
    }
}