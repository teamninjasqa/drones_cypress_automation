class editProjectRequest {
    constructor({ edit_pilot_name, approve_project_status, edit_project_status, edit_reviewer_comments, aircraft_name, edit_project_name, edit_project_purpose, edit_uas_regulation, edit_project_start_date, edit_project_end_date, edit_field_time, edit_frequency, edit_expected_range, edit_latitude, edit_longitude, edit_max_distance, edit_flight_altitude, edit_condition_index, edit_flightProcedure, edit_op_restriction_rain, edit_op_restriction_trees, edit_op_Conflicting, edit_risk_assessment, edit_user_comments, edit_observer }) {
        this.aircraft_name = aircraft_name;
        this.edit_pilot_name = edit_pilot_name;
        this.edit_project_name = edit_project_name;
        this.edit_project_purpose = edit_project_purpose;
        this.edit_uas_regulation = edit_uas_regulation;
        this.edit_project_start_date = edit_project_start_date;
        this.edit_project_end_date = edit_project_end_date;
        this.edit_field_time = edit_field_time;
        this.edit_frequency = edit_frequency;
        this.edit_expected_range = edit_expected_range;
        this.edit_latitude = edit_latitude;
        this.edit_longitude = edit_longitude;
        this.edit_max_distance = edit_max_distance;
        this.edit_flight_altitude = edit_flight_altitude;
        this.edit_condition_index = edit_condition_index;
        this.edit_flightProcedure = edit_flightProcedure;
        this.edit_op_restriction_rain = edit_op_restriction_rain;
        this.edit_op_restriction_trees = edit_op_restriction_trees;
        this.edit_op_Conflicting = edit_op_Conflicting;
        this.edit_risk_assessment = edit_risk_assessment;
        this.edit_user_comments = edit_user_comments;
        this.edit_observer = edit_observer;
        this.edit_reviewer_comments = edit_reviewer_comments;
        this.edit_project_status = edit_project_status;
        this.approve_project_status = approve_project_status;
        // this.edit_submit_request = edit_submit_request;

    }
}
export default editProjectRequest;