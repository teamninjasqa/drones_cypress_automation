class manageFlight {

    constructor({ manage_flight_url, mfd_aircraft_name, mfd_pilot_name, mfd_nickname, mfd_flight_purpose, mfd_uas_regulation, mfd_flight_date, mfd_flight_time, mfd_field_time, mfd_no_of_flights, mfd_latitude, mfd_longitude, mfd_max_distance, mfd_flight_altitude, mfd_condition_index, mfd_flightProcedure, mfd_op_restriction_rain, mfd_op_restriction_trees, mfd_op_restriction_wind, mfd_op_Conflicting, mfd_op_TRFs, mfd_op_NOTAMS, mfd_observer, mfd_risk_assessment, mfd_user_comments }) {
        this.manage_flight_url = manage_flight_url;
        this.mfd_aircraft_name = mfd_aircraft_name;
        this.mfd_pilot_name = mfd_pilot_name;
        this.mfd_nickname = mfd_nickname;
        this.mfd_flight_purpose = mfd_flight_purpose;
        this.mfd_uas_regulation = mfd_uas_regulation;
        this.mfd_flight_date = mfd_flight_date;
        this.mfd_flight_time = mfd_flight_time;
        this.mfd_field_time = mfd_field_time;
        this.mfd_no_of_flights = mfd_no_of_flights;
        this.mfd_latitude = mfd_latitude;
        this.mfd_longitude = mfd_longitude;
        this.mfd_max_distance = mfd_max_distance;
        this.mfd_flight_altitude = mfd_flight_altitude;
        this.mfd_condition_index = mfd_condition_index;
        this.mfd_flightProcedure = mfd_flightProcedure;
        this.mfd_op_restriction_rain = mfd_op_restriction_rain;
        this.mfd_op_restriction_trees = mfd_op_restriction_trees;
        this.mfd_op_restriction_wind = mfd_op_restriction_wind;
        this.mfd_op_Conflicting = mfd_op_Conflicting;
        this.mfd_op_TRFs = mfd_op_TRFs;
        this.mfd_op_NOTAMS = mfd_op_NOTAMS;
        this.mfd_observer = mfd_observer;
        this.mfd_risk_assessment = mfd_risk_assessment;
        this.mfd_user_comments = mfd_user_comments;
    }
}
export default manageFlight;