class createAircraft {

    constructor({ cr_manage_flight_url, cr_campus_index, cr_nickname, cr_storage_location, cr_faa_registration, cr_university_name, cr_manufacturer_name, cr_responsible_person, cr_model_name }) {
        this.cr_manage_flight_url = cr_manage_flight_url;
        this.cr_university_name = cr_university_name;
        this.cr_responsible_person = cr_responsible_person;
        this.cr_manufacturer_name = cr_manufacturer_name;
        this.cr_model_name = cr_model_name;
        this.cr_faa_registration = cr_faa_registration;
        this.cr_storage_location = cr_storage_location;
        this.cr_nickname = cr_nickname;
        this.cr_campus_index = cr_campus_index;
    }
}
export default createAircraft;