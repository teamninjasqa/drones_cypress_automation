class updateAircraft {

    constructor({ up_campus_index, up_nickname, up_storage_location, up_faa_registration, up_university_name, up_manufacturer_name, up_responsible_person, up_model_name }) {
        this.up_university_name = up_university_name;
        this.up_responsible_person = up_responsible_person;
        this.up_manufacturer_name = up_manufacturer_name;
        this.up_model_name = up_model_name;
        this.up_faa_registration = up_faa_registration;
        this.up_storage_location = up_storage_location;
        this.up_nickname = up_nickname;
        this.up_campus_index = up_campus_index;
    }
}
export default updateAircraft;