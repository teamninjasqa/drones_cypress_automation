class updateFlight {
    constructor({ ed_mfd_pilot_name, ed_mfd_nickname, ed_mfd_flight_purpose, ed_mfd_uas_regulation, ed_mfd_flight_date, ed_mfd_flight_time, ed_mfd_field_time, ed_mfd_no_of_flights, ed_mfd_latitude, ed_mfd_longitude, ed_mfd_max_distance, ed_mfd_flight_altitude, ed_mfd_condition_index, ed_mfd_flightProcedure, ed_mfd_op_Conflicting, ed_mfd_op_TRFs, ed_mfd_op_NOTAMS, ed_mfd_observer, ed_mfd_risk_assessment, ed_mfd_user_comments }) {
        this.ed_mfd_pilot_name = ed_mfd_pilot_name;
        this.ed_mfd_nickname = ed_mfd_nickname;
        this.ed_mfd_flight_purpose = ed_mfd_flight_purpose;
        this.ed_mfd_uas_regulation = ed_mfd_uas_regulation;
        this.ed_mfd_flight_date = ed_mfd_flight_date;
        this.ed_mfd_flight_time = ed_mfd_flight_time;
        this.ed_mfd_field_time = ed_mfd_field_time;
        this.ed_mfd_no_of_flights = ed_mfd_no_of_flights;
        this.ed_mfd_latitude = ed_mfd_latitude;
        this.ed_mfd_longitude = ed_mfd_longitude;
        this.ed_mfd_max_distance = ed_mfd_max_distance;
        this.ed_mfd_flight_altitude = ed_mfd_flight_altitude;
        this.ed_mfd_condition_index = ed_mfd_condition_index;
        this.ed_mfd_flightProcedure = ed_mfd_flightProcedure;
        this.ed_mfd_op_Conflicting = ed_mfd_op_Conflicting;
        this.ed_mfd_op_TRFs = ed_mfd_op_TRFs;
        this.ed_mfd_op_NOTAMS = ed_mfd_op_NOTAMS;
        this.ed_mfd_observer = ed_mfd_observer;
        this.ed_mfd_risk_assessment = ed_mfd_risk_assessment;
        this.ed_mfd_user_comments = ed_mfd_user_comments;
    }
}
export default updateFlight;