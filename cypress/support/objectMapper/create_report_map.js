class createAircraft {

    constructor({ cr_manage_report_url, cr_search_name, other_accident, lostlink_details, malfunction_details, cr_Duration_per_flight, cr_flight_count, cr_risk_assessment, cr_observer, cr_takeoff_damages }) {
        this.cr_manage_report_url = cr_manage_report_url;
        this.cr_search_name = cr_search_name;
        this.cr_Duration_per_flight = cr_Duration_per_flight;
        this.cr_flight_count = cr_flight_count;
        this.cr_risk_assessment = cr_risk_assessment;
        this.cr_observer = cr_observer;
        this.cr_takeoff_damages = cr_takeoff_damages;
        this.malfunction_details = malfunction_details;
        this.lostlink_details = lostlink_details;
        this.other_accident = other_accident;


    }
}
export default createAircraft;