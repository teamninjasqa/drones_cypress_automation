class createProject {
    constructor({ pilot_name, aircraft_name, d_reviewer_comments, dev_project_create, d_project_name, d_project_purpose, d_uas_regulation, d_project_start_date, d_project_end_date, d_field_time, d_frequency, d_expected_range, d_latitude, d_longitude, d_max_distance, d_flight_altitude, d_condition_index, d_flightProcedure, d_op_restriction_rain, d_op_restriction_trees, d_op_restriction_wind, d_op_Conflicting, d_op_TRFs, d_op_NOTAMS, d_observer, d_risk_assessment, d_user_comments }) {
        this.dev_project_create = dev_project_create;
        this.aircraft_name = aircraft_name;
        this.pilot_name = pilot_name;
        this.d_project_name = d_project_name;
        this.d_project_purpose = d_project_purpose;
        this.d_uas_regulation = d_uas_regulation;
        this.d_project_start_date = d_project_start_date;
        this.d_project_end_date = d_project_end_date;
        this.d_field_time = d_field_time;
        this.d_frequency = d_frequency;
        this.d_expected_range = d_expected_range;
        this.d_latitude = d_latitude;
        this.d_longitude = d_longitude;
        this.d_max_distance = d_max_distance;
        this.d_flight_altitude = d_flight_altitude;
        this.d_condition_index = d_condition_index;
        this.d_flightProcedure = d_flightProcedure;
        this.d_op_restriction_rain = d_op_restriction_rain;
        this.d_op_restriction_trees = d_op_restriction_trees;
        this.d_op_restriction_wind = d_op_restriction_wind;
        this.d_op_Conflicting = d_op_Conflicting;
        this.d_op_TRFs = d_op_TRFs;
        this.d_op_NOTAMS = d_op_NOTAMS;
        this.d_observer = d_observer;
        this.d_risk_assessment = d_risk_assessment;
        this.d_user_comments = d_user_comments;
        // this.d_submit_request = d_submit_request;

    }
}
export default createProject;