class editPilot {
    constructor({ d_aircraft_name, d_campus_selection, d_pilot_to_create, d_cert_type, d_pilot_phone, d_cert_no, d_pilot_name, d_inline_select }) {
        this.d_aircraft_name = d_aircraft_name;
        this.d_pilot_name = d_pilot_name;
        this.d_campus_selection = d_campus_selection;
        this.d_pilot_to_create = d_pilot_to_create;
        this.d_cert_type = d_cert_type;
        this.d_cert_no = d_cert_no;
        this.d_pilot_phone = d_pilot_phone;
        this.d_inline_select = d_inline_select;
    }
}
export default editPilot;