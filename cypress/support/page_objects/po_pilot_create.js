module.exports = {
    pilot_creation:
    {
        "create_pilot_url": 'https://dev.riskandsafety.com/drones/pilot',
        "validate_homepage": '[href="/drones/"]',
        "create_button_pilot": '[href="/drones/pilot"]',
        "create_plus_button": '[aria-label="Add Pilot"]',
        "select_campus": '#campus',
        "serach_pilots": '#search',
        "enter_pilot_tocreate": "#searchPerson",
        "select_pilot_autopop": "Garcia, Juan Carlos",
        "list_option": 'option[value="03"]',
    },
    pilot_details:
    {
        "certification_type": "#certificateType",
        "certificate_name": "#certificate",
        "pilot_phone": "#phone",
        "Qualify_GS": "Ground Station",
        "Qualify_pilot": "A person who manipulates the flight controls of the aircraft",
        "submit_pilot": "[type='submit']"
    },
    search_pilot:
    {
        "pilot_name_autopop": ".inline"
    },
    edit_pilot_QF:
    {
        'Qualify_instructor': " instruction to a trainee"
    }
}