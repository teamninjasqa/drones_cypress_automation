module.exports = {
    navigate:
    {
        "add_flight_plusbtn":"[aria-label='Add Flight']"
    },

    Pilot_flight_details:
    {
        'enter_aircraft_name': '#searchAircraft',
        'aircraft_autopop_sel': '.text-muted',
        'pilot_search_name': '#search',

    },
    Project_request:
    {
        'nickName': '#nickName',
        'project_purpose': '#purpose',
        'uas_regulation': '#partNumber',
        'Flight_date': '#date',
        'flight_time': '#time',
        'Expected_field_time': '#fieldTime' ,
        'latitude': '#latitude',
        'longitude': '#longitude',
        'no_of_flights': '#flights',
        'flight_distance': '#distance',
        'flight_altitude': '#altitude',
        'flying_over': 'input[name="overPeople"]',
        'near_building': 'input[name="nearBuildings"]',
        'flying_indoor': 'input[name="indoor"]',
        'flight_procedure': '#flightProcedure',
        'risk_assessment': '#riskAssessment',
        'observers_details': '#observers',
        'user_comments':'#comments',
        'save_flight': 'button[type="submit"]',
        'edit_project': 'Edit Request',
        'submit_request':"Submit Request"
    }
}