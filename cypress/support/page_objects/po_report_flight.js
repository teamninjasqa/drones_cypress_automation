module.exports = {
    'create_aircraft': {
        'pilot_details': '#pilot',
        'plus_button': "[aria-label='Add Flight']",
        'list_wait': '.list',
        'add_flight_new': 'Add Flight',
    },
    create_report:
    {
        'create_report_button': "Create Report",
        'Duration_of_flights': "[type='number']",
        'incr_flight_count': ".input-group-addon"
    },
    Project_request:
    {
        'nickName': '#nickName',
        'uas_regulation': '#partNumber',
        'Flight_date': '#date',
        'flight_time': '#time',
        'latitude': '#latitude',
        'longitude': '#longitude',
        "add_flight_plusbtn": '.input-group-addon',
        'duration_per_flight': 'input[placeholder="in minutes"]',
        'risk_assessment': '#riskAssessment',
        'observers_details': '#observers',
        'takeoff_damages': '#damages',
        'save_flight_details': 'button[type="submit"]',
        'edit_project': 'Edit Request',
        'submit_request': "Submit Request"
    },
    equipment_malfunction:
    {
        'onboard': 'On-board flight control system',
        'fuel_system': 'Fuel system failure',
        'control_system': 'Control station Failure',
        'aircraft_collision': 'Aircraft collision involving another aircraft',
        'other_comments': 'Other',
        'other_malfunction': '#otherMalfunction'
    },
    lost_link:
    {
        'pilot_control': 'Lost link of pilot control',
        'telemetry_system': 'Lost link of payload telemetry system',
        'Fly_away': 'Fly-away resulting in flight termination',
        'unplanned_lost': 'Execution of a unplanned lost link procedure',
        'other_comments': 'Other',
        'other_malfunction': '#eventNotes'
    },
    accidents_ishaps:
    {
        'unmanned_aircraft': 'Damage to property other than the unmanned aircraft',
        'aircraft_loss': 'Total aircraft loss',
        'serious_injury': 'Serious injury',
        'fatal_injury': 'Fatal injury',
        'other_comments': 'Other',
        'other_accidents': '#otherAccidents'
    }

}