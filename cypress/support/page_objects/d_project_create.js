
module.exports = {
    Create:
    {
        'create_prj_btn': '[aria-label="Add Project"]',
        'enter_aircraft_name': '#searchAircraft',
        'aircraft_autopop_sel': '.text-muted',
        'pilot_search_name': '#search',
        'verify_page': '[href="/drones/"]'

    },

    Project_request:
    {
        'project_name': '#projectName',
        'project_purpose': '#purpose',
        'uas_regulation': '#partNumber',
        'project_start_date': '#startdate',
        'project_end_date': '#enddate',
        'field_time': '#fieldTime',
        'frequency': '#frequency',
        'expected_range': '#range',
        'latitude': '#latitude',
        'longitude': '#longitude',
        'distance_cover': '#distance',
        'project_altitude': '#altitude',
        'flying_over': 'input[name="overPeople"]',
        'near_building': 'input[name="nearBuildings"]',
        'flying_indoor': 'input[name="indoor"]',
        'flight_procedure': '#flightProcedure',
        'risk_assessment': '#riskAssessment',
        'observers_details': '#observers',
        'user_comments': '#comments',
        'submit_project': 'button[type="submit"]',
        'edit_project': 'Edit Request',
        'submit_request': "Submit Request",
        'submit_comments': ".form-control",
        'risk_score': 'Moderate Risk',
        'approve_risk': 'Approve Request'
    },
    inreview: {
        'search_project': '#pilot',
        'project_status': '#status',
        'list_wait': '.list',        
    }

}

