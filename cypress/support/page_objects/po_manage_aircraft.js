module.exports = {
    'create_aircraft': {
        'plus_button': '[aria-label="Add Aircraft"]',
        'manufacturer': 'input[aria-label="manufacturer"]',
        'university_search': '#searchPerson',
        'aircraft_autopop_sel': '.text-muted',
        'aircraft_model': 'input[aria-label="model"]',
        "faa_registration": '#registration',
        'storage_location': '#storageLocation',
        'campus_owned': '#ucOwned',
        'nick_name': '#nickName',
        'save_button': 'button[type="submit"]',
    },
    
    'update_aircraft':
    {
        'search_aircraft': '#search',
        'list_wait':'.list',
        'change_owner': 'Change Owner'
    }

}