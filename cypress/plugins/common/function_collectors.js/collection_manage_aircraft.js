import UiAction from '../page_actions'
import manageAircraftObj from '../../../support/page_objects/po_manage_aircraft'

class manageAircraftCollection {

    pageActions = new UiAction();

    navigateManageAircraft = (url) => {
        this.pageActions.doVisit(url);
        this.pageActions.getElement(manageAircraftObj.update_aircraft.list_wait);
        this.pageActions.getElement(manageAircraftObj.create_aircraft.plus_button);
    }
    addManageAircraft = (manufacturerName, responsiblePerson, aircrafModel, faaRegistration, storageLocation, campus_indexval, nickName) => {
        this.pageActions.clickButton(manageAircraftObj.create_aircraft.plus_button);
        this.pageActions.enterText(manageAircraftObj.create_aircraft.manufacturer, manufacturerName);
        this.pageActions.enterText(manageAircraftObj.create_aircraft.university_search, responsiblePerson)
        this.pageActions.clickButton(manageAircraftObj.create_aircraft.aircraft_autopop_sel);
        this.pageActions.enterText(manageAircraftObj.create_aircraft.aircraft_model, aircrafModel);
        this.pageActions.enterText(manageAircraftObj.create_aircraft.faa_registration, faaRegistration);
        this.pageActions.enterText(manageAircraftObj.create_aircraft.storage_location, storageLocation);
        this.pageActions.clickButton(manageAircraftObj.create_aircraft.campus_owned);
        // this.pageActions.radioButton(manageAircraftObj.create_aircraft.campus_owned, campus_indexval);
        this.pageActions.enterText(manageAircraftObj.create_aircraft.nick_name, nickName)

    }
    submitAircraft = () => {
        this.pageActions.clickButton(manageAircraftObj.create_aircraft.save_button);
        this.pageActions.getElement(manageAircraftObj.create_aircraft.plus_button);
    }

    updateManageAircraft = (searchNickName, manufacturerName, responsiblePerson, aircrafModel, faaRegistration, storageLocation, nickName) => {
        this.pageActions.enterText(manageAircraftObj.update_aircraft.search_aircraft, searchNickName)
        this.pageActions.containsClick(searchNickName);
        this.pageActions.EnterTextClear(manageAircraftObj.create_aircraft.manufacturer, manufacturerName);
        this.pageActions.containsClick(manageAircraftObj.update_aircraft.change_owner);
        this.pageActions.EnterTextClear(manageAircraftObj.create_aircraft.university_search, responsiblePerson)
        this.pageActions.clickButton(manageAircraftObj.create_aircraft.aircraft_autopop_sel);
        this.pageActions.EnterTextClear(manageAircraftObj.create_aircraft.aircraft_model, aircrafModel);
        this.pageActions.EnterTextClear(manageAircraftObj.create_aircraft.faa_registration, faaRegistration);
        this.pageActions.EnterTextClear(manageAircraftObj.create_aircraft.storage_location, storageLocation);
        this.pageActions.clickButton(manageAircraftObj.create_aircraft.campus_owned);
        this.pageActions.EnterTextClear(manageAircraftObj.create_aircraft.nick_name, nickName)
    }

}

export default manageAircraftCollection