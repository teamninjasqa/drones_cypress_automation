import CustomAction from '../page_actions';

import manageFlightObj from '../../../support/page_objects/po_manage_flight';
// import manageFlight from '../../../support/objectMapper/manage_flight_map'

class manageFlightCollection {

    pageActions = new CustomAction();

    /* navigateFlight = (flighturl) => {
        this.pageActions.getElement(manageFlightObj.navigate.add_flight_plusbtn);
    } */

    addManageFlight = (flighturl, aircraftName, pilotName, nickName, purpose, uasRegulation, flightDate, flightTime, fieldTime, flightCount, latitude, longitude, maxDistance, altitude, conditionIndex, procedure, wind, rain, tree, riskAssessment, observers, comments) => {
        this.pageActions.doVisit(flighturl);
        this.pageActions.getElement(manageFlightObj.navigate.add_flight_plusbtn);
        this.pageActions.clickButton(manageFlightObj.navigate.add_flight_plusbtn);
        this.pageActions.enterText(manageFlightObj.Pilot_flight_details.enter_aircraft_name, aircraftName);
        this.pageActions.clickButton(manageFlightObj.Pilot_flight_details.aircraft_autopop_sel);
        this.pageActions.enterText(manageFlightObj.Pilot_flight_details.pilot_search_name, pilotName);
        this.pageActions.clickButton(manageFlightObj.Pilot_flight_details.aircraft_autopop_sel);
        this.pageActions.enterText(manageFlightObj.Project_request.nickName, nickName);
        this.pageActions.listSelect(manageFlightObj.Project_request.project_purpose, purpose);
        this.pageActions.listSelect(manageFlightObj.Project_request.uas_regulation, uasRegulation);
        this.pageActions.enterText(manageFlightObj.Project_request.Flight_date, flightDate);
        this.pageActions.enterText(manageFlightObj.Project_request.flight_time, flightTime);
        this.pageActions.enterText(manageFlightObj.Project_request.Expected_field_time, fieldTime);
        this.pageActions.enterText(manageFlightObj.Project_request.no_of_flights, flightCount);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.latitude, latitude);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.longitude, longitude);
        this.pageActions.enterText(manageFlightObj.Project_request.flight_distance, maxDistance);
        this.pageActions.enterText(manageFlightObj.Project_request.flight_altitude, altitude);
        this.pageActions.radioButton(manageFlightObj.Project_request.flying_over, conditionIndex);
        this.pageActions.radioButton(manageFlightObj.Project_request.flying_indoor, conditionIndex);
        this.pageActions.radioButton(manageFlightObj.Project_request.near_building, conditionIndex);
        this.pageActions.enterText(manageFlightObj.Project_request.flight_procedure, procedure);
        this.pageActions.containsClick(wind);
        this.pageActions.containsClick(rain);
        this.pageActions.containsClick(tree);
        this.pageActions.enterText(manageFlightObj.Project_request.risk_assessment, riskAssessment);
        this.pageActions.enterText(manageFlightObj.Project_request.observers_details, observers);
        this.pageActions.enterText(manageFlightObj.Project_request.user_comments, comments);
        this.pageActions.clickButton(manageFlightObj.Project_request.save_flight);
    }

    updateManageFlight = (pilotName, nickName, purpose, uasRegulation, flightDate, flightTime, fieldTime, flightCount, latitude, longitude, maxDistance, altitude, conditionIndex, procedure, conflicting, trfs, notams, observers, riskAssessment, comments) => {
        this.pageActions.containsClick(manageFlightObj.Project_request.edit_project);
        this.pageActions.enterText(manageFlightObj.Pilot_flight_details.pilot_search_name, pilotName);
        this.pageActions.clickButton(manageFlightObj.Pilot_flight_details.aircraft_autopop_sel);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.nickName, nickName);
        this.pageActions.listSelect(manageFlightObj.Project_request.project_purpose, purpose);
        this.pageActions.listSelect(manageFlightObj.Project_request.uas_regulation, uasRegulation);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.Flight_date, flightDate);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.flight_time, flightTime);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.Expected_field_time, fieldTime);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.no_of_flights, flightCount);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.latitude, latitude);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.longitude, longitude);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.flight_distance, maxDistance);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.flight_altitude, altitude);
        this.pageActions.radioButton(manageFlightObj.Project_request.flying_over, conditionIndex);
        this.pageActions.radioButton(manageFlightObj.Project_request.flying_indoor, conditionIndex);
        this.pageActions.radioButton(manageFlightObj.Project_request.near_building, conditionIndex);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.flight_procedure, procedure);
        this.pageActions.containsClick(conflicting);
        this.pageActions.containsClick(trfs);
        this.pageActions.containsClick(notams);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.risk_assessment, riskAssessment);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.observers_details, observers);
        this.pageActions.EnterTextClear(manageFlightObj.Project_request.user_comments, comments);
        this.pageActions.clickButton(manageFlightObj.Project_request.save_flight);
    }

    submitFlightReqest = () => {
        this.pageActions.containsClick(manageFlightObj.Project_request.submit_request);
        this.pageActions.getElement(manageFlightObj.navigate.add_flight_plusbtn);
    }
}

export default manageFlightCollection;