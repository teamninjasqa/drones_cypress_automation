import UiAction from '../page_actions'
import reportAircraftObj from '../../../support/page_objects/po_report_flight'

class manageAircraftCollection {

    pageActions = new UiAction();

    navigateManageAircraft = (url) => {
        this.pageActions.doVisit(url);
        this.pageActions.getElement(reportAircraftObj.create_aircraft.plus_button)
    }

    searchManageAircraft = (projectName) => {
        this.pageActions.enterText(reportAircraftObj.create_aircraft.pilot_details, projectName);
        this.pageActions.getElement(reportAircraftObj.create_aircraft.list_wait);
        this.pageActions.containsClick(projectName)
    }

    addNewFlight = (date, time, flightCount, assessment, observer, comments) => {
        this.pageActions.containsClick(reportAircraftObj.create_aircraft.add_flight_new);
        this.pageActions.getElement(reportAircraftObj.Project_request.save_flight_details);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.Flight_date, date);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.flight_time, time);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.no_of_flights, flightCount);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.risk_assessment, assessment);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.observers_details, observer);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.user_comments, comments);

    }

    createAircraftReport = (flightCount, flightDuration, assessment, observer, comments) => {
        this.pageActions.containsClick(reportAircraftObj.create_report.create_report_button);
        this.pageActions.getElement(reportAircraftObj.Project_request.save_flight_details);
        // this.pageActions.EnterTextClear(reportAircraftObj.Project_request.Flight_date, date);
        // this.pageActions.EnterTextClear(reportAircraftObj.Project_request.flight_time, time);
        this.pageActions.getElement(reportAircraftObj.Project_request.add_flight_plusbtn)
        for (let index = 0; index < flightCount - 1; index++) {
            cy.log("entry index  :" + index)
            // const element = array[index];
            if (index > 0) {
                cy.log('entered in if')
                this.pageActions.clickButton(reportAircraftObj.Project_request.add_flight_plusbtn)
                cy.log('click done and move next')
            }
            cy.log('exit from if')
            this.pageActions.enterTextIndex(reportAircraftObj.Project_request.duration_per_flight, flightDuration, index);
            cy.log("completed   :" + index)

        }
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.risk_assessment, assessment);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.observers_details, observer);
        this.pageActions.EnterTextClear(reportAircraftObj.Project_request.takeoff_damages, comments);
    }

    userEquipmentMalfunctionsReport = (malfunction) => {
        this.pageActions.containsClick(reportAircraftObj.equipment_malfunction.onboard);
        this.pageActions.containsClick(reportAircraftObj.equipment_malfunction.aircraft_collision);
        this.pageActions.containsClick(reportAircraftObj.equipment_malfunction.control_system);
        this.pageActions.containsClick(reportAircraftObj.equipment_malfunction.fuel_system);
        this.pageActions.containsClick(reportAircraftObj.equipment_malfunction.other_comments);
        this.pageActions.enterText(reportAircraftObj.equipment_malfunction.other_malfunction, malfunction);
    }

    userLostLinkEvents = (lostlink) => {
        this.pageActions.containsClick(reportAircraftObj.lost_link.pilot_control);
        this.pageActions.containsClick(reportAircraftObj.lost_link.telemetry_system);
        this.pageActions.containsClick(reportAircraftObj.lost_link.Fly_away);
        this.pageActions.containsClick(reportAircraftObj.lost_link.unplanned_lost);
        this.pageActions.containsClick(reportAircraftObj.lost_link.other_comments);
        this.pageActions.enterText(reportAircraftObj.lost_link.other_comments, lostlink);
    }

    userAccedentMishapReport = (accidents) => {
        this.pageActions.containsClick(reportAircraftObj.accidents_ishaps.unmanned_aircraft);
        this.pageActions.containsClick(reportAircraftObj.accidents_ishaps.aircraft_loss);
        this.pageActions.containsClick(reportAircraftObj.accidents_ishaps.serious_injury);
        this.pageActions.containsClick(reportAircraftObj.accidents_ishaps.fatal_injury);
        this.pageActions.containsClick(reportAircraftObj.accidents_ishaps.other_comments);
        this.pageActions.enterText(reportAircraftObj.accidents_ishaps.other_accidents, accidents)
    }

    testrandom = function randommath () {
        var textArray = ["[data-value='Non-Conforming']", "[data-value='Partially Conforming']", "[data-value='Substantially Conforming']", "[data-value='Conforming']", "[data-value='NA']"];
        return textArray[Math.floor(Math.random() * textArray.length)];
    }

}

export default manageAircraftCollection